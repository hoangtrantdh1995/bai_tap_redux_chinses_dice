import { CHOOSE_DICE, PLAY_GAME, RESULT } from "../constant/diceConstant";


let initialState = {
    arrayDice: [
        {
            img: "./imgXucXac/1.png",
            value: 1
        },
        {
            img: "./imgXucXac/2.png",
            value: 2
        },
        {
            img: "./imgXucXac/3.png",
            value: 3
        },
        // {
        //     img: "./imgXucXac/4.png",
        //     value: 4
        // },
        // {
        //     img: "./imgXucXac/5.png",
        //     value: 5
        // },
        // {
        //     img: "./imgXucXac/6.png",
        //     value: 5
        // },
    ],
    totalWin: 0,
    totalLose: 0,
    totalPlayGame: 0,
    chooseDice: "",
    resultPlayGame: "",
};

export const diceReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case CHOOSE_DICE: {
            return { ...state, chooseDice: payload }

        }
        case RESULT: {
            // tạo array mới 
            let newArrayDice = state.arrayDice.map(() => {
                // Math.floor chỉ trả từ 0 đến 0.9 nên phải cộng 1
                let random = Math.floor(Math.random() * 6) + 1;
                return {
                    img: `./imgXucXac/${random}.png`,
                    value: random,
                }
            })

            const point = newArrayDice.reduce((total, item) => {
                total += item.value;
                return total
            }, 0)

            let totalWin = state.totalWin
            let totalLose = state.totalLose
            let totalPlayGame = state.totalPlayGame
            let resultPlayGame = 'You LOSE'
            if (state.chooseDice == 'TAI' && point >= 11) {
                totalWin++
                resultPlayGame = 'You WIN'
            } else if (state.chooseDice == 'XIU' && point < 11) {
                totalWin++
                resultPlayGame = 'You WIN'
            } else {
                totalLose++
            }
            totalPlayGame = totalWin + totalLose
            return { ...state, arrayDice: newArrayDice, resultPlayGame, totalWin, totalLose, totalPlayGame }
        }

        default:
            return state
    }
};


