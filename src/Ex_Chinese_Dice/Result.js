import React, { Component } from 'react'
import { connect } from 'react-redux'
import { playGame, result } from '../redux/action/diceAction'

class Result extends Component {

    // playGame = () => {
    //     // tạo array mới 
    //     let newArrayDice = this.props.arrayDice.map(() => {
    //         // Math.floor chỉ trả từ 0 đến 0.9 nên phải cộng 1
    //         let random = Math.floor(Math.random() * 6) + 1;
    //         return {
    //             img: `./imgXucXac/${random}.png`,
    //             value: random,
    //         }
    //     })
    //     // sau 5s
    //     this.props.showResult(newArrayDice)
    // }

    render() {
        return (
            <div className='text-center pt-3 display-4'>
                <button
                    onClick={this.props.handleResult}
                    className='btn btn-secondary btn-lg'>
                    <span>Play Game</span>
                </button>
                <div>
                    <h2 className='text-primary'>{this.props.resultPlayGame}</h2>
                </div>
                <div className='pt-1'>
                    <h2>Bạn chọn: {this.props.chooseDice}</h2>
                    <h3>Số lần bạn thắng : {this.props.totalWin} </h3>
                    <h3>Số lần bạn thua :{this.props.totalLose} </h3>
                    <h3>Số lần play game : {this.props.totalPlayGame}</h3>
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => ({
    // arrayDice: state.diceReducer.arrayDice,
    resultPlayGame: state.diceReducer.resultPlayGame,
    totalWin: state.diceReducer.totalWin,
    totalLose: state.diceReducer.totalLose,
    totalPlayGame: state.diceReducer.totalPlayGame,
    chooseDice: state.diceReducer.chooseDice,

})

let mapDispatcchToProps = (dispatch) => ({
    handleResult: () => {
        dispatch(result())
    },
    // showResult: (dice) => {
    //     dispatch(playGame(dice))
    // }
})

export default connect(mapStateToProps, mapDispatcchToProps)(Result);
