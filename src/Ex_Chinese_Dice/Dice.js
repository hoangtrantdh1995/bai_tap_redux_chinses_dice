import React, { Component } from 'react'
import { connect } from 'react-redux'
import { chooseDice } from '../redux/action/diceAction'
import { TAI, XIU } from '../redux/constant/diceConstant'


function ButtonComponent({ text, onClick, value, active, color }) {
    const className = `btn btn-${color} col-2`
    return (<button style={{
        width: 150,
        height: 150,
        fontSize: 60,
        transform: `scale(${active == value ? 1.5 : 1})`
    }}
        onClick={() => onClick(value)}
        className={className}>{text}</button>)
}

class Dice extends Component {
    handleChangeActive = (value) => {
        this.props.handleChooseDice(value)
    }

    renderDice = () => {
        return this.props.arrayDice.map((item, index) =>
            <img
                src={item.img}
                alt=""
                style={{ width: 100, margin: 15, }}
                className="rounded"
                key={index} />)
    }

    render() {
        return (
            <div className='container pt-3'>
                <h1 style={{ fontSize: 70 }}>Game Tài Xỉu</h1>
                <div className='d-flex justify-content-between pt-3'>
                    {/* <button style={{
                        width: 150,
                        height: 150,
                        fontSize: 60,
                        transform: `scale(${this.props.chooseDice == TAI ? 1.5 : 1})`
                    }}
                        onClick={() => { this.handleChangeActive(TAI) }}
                        className='btn btn-danger col-2'>Tài</button> */}
                    <ButtonComponent
                        text="Tài"
                        value={TAI}
                        active={this.props.chooseDice}
                        onClick={this.handleChangeActive}
                        color="danger"
                    />
                    <div>{this.renderDice()}</div>
                    <ButtonComponent
                        text="Xỉu"
                        value={XIU}
                        active={this.props.chooseDice}
                        onClick={this.handleChangeActive}
                        color="dark"
                    />
                    {/* <button style={{
                        width: 150,
                        height: 150,
                        fontSize: 60,
                        transform: `scale(${this.props.chooseDice == XIU ? 1.5 : 1})`
                    }}
                        onClick={() => { this.handleChangeActive(XIU) }}
                        className='btn btn-dark col-2'>Xỉu</button> */}
                </div>
            </div>
        )
    }
};

const mapStateToProps = (state) => ({
    arrayDice: state.diceReducer.arrayDice,
    chooseDice: state.diceReducer.chooseDice,
})

const mapDispatcchToProps = (dispatch) => ({
    handleChooseDice(dice) {
        dispatch(chooseDice(dice));
    }
})

export default connect(mapStateToProps, mapDispatcchToProps)(Dice)
